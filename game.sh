#!/bin/sh
shopt -s nocasematch # turn of case sensitivity

#variables (for easy customization)
cryLimit=5 # amount of times you can cry before the ogre gets angry
saveFolder="./data" #sets what directory to save save data in

if [ -d $saveFolder ] # check to see if save directory exists
then
	#code breaks if then section of if statement doesn't have a command
	echo "what" > /dev/null
else
	mkdir $saveFolder
	#add starting variables here
	echo 0 > $saveFolder/friendship
	echo 0 > $saveFolder/sword
	echo 0 > $saveFolder/deaths
	echo 0 > $saveFolder/wins
fi

die()
{
	echo "The ogre stabs you and you die."
	deathCount=$(cat $saveFolder/deaths)
	((deathCount++))
	echo $deathCount > $saveFolder/deaths
}
win()
{
	echo "You win" > /dev/null # not sure what to have victory message
	winCount=$(cat $saveFolder/wins)
	((winCount++))
	echo $winCount > $saveFolder/wins
}



echo "A foul ogre lays before you, ready to strike!"
read -p "What do you do? " input
input=${input,,}
case $input in
	"stab him")
		sword=$(cat $saveFolder/sword)
		if [ $sword -eq 0 ]; then
			echo "With what?"
			die
		else
			echo "The ogre falls!"
			win
		fi
		;;
	"grab sword")
		sword=$(cat $saveFolder/sword)
		if [ $sword -eq 0 ]; then
			echo 1 > $saveFolder/sword
			echo "You pick up the sword, but the ogre strikes you down before you can defend yourself"
			die
		else
			echo "You already have one!"
			echo "The ogre notices you're distracted"
			die
		fi
		;;
	"run away")
		echo "The ogre chases after you!"
		win
		;;
	"cry")
		friendship=$(cat $saveFolder/friendship) # check friendship value
		if [ $cryLimit -ge $friendship ]; then # test if friendship value is less than 5
			((friendship++)) # increase friendship value by 1
			echo $friendship > $saveFolder/friendship # save friendship value

			echo "You curl into a ball and lay down."
			echo "The ogre understands your pain."
			echo "After a long chat, the ogre calms you down and befriends you!"
			win
		else
			echo "The ogre is tired of your crying despite you being so lucky as to being friends with him."
			die
		fi
		;;
	*)
	if [ -z "$input" ]; then # check if no input was typed
		echo "You are paralyzed by fear!"
		die
	else # only activates if unknown command was typed
		echo "I can't do that!"
		die
	fi
	;;
esac
