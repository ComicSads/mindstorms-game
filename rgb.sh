#!/bin/bash
# gpio11 is red
# gpio4 is green
# gpio17 is blue

#echo 11 > /sys/class/gpio/export
#echo out > /sys/class/gpio/gpio11/direction
#echo 0 > /sys/class/gpio/gpio11/value
#echo 11 > /sys/class/gpio/unexport

#echo 4 > /sys/class/gpio/export
#echo out > /sys/class/gpio/gpio4/direction
#echo 0 > /sys/class/gpio/gpio4/value
#echo 4 > /sys/class/gpio/unexport

#echo 17 > /sys/class/gpio/export
#echo out > /sys/class/gpio/gpio17/direction
#echo 0 > /sys/class/gpio/gpio17/value
#echo 17 > /sys/class/gpio/unexport

echo 11 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio11/direction
light=/sys/class/gpio/gpio11/value
echo 0 > $light

unit=1 # length of time one dot takes up
dah=$((unit * 3)) # one dash is 3 units
letter=$dah # the space between letters is 3 units
dit=$unit # a dot is 1 unit
sameL=$dit # the space between parts of the same letter is 1 units
word=$(($unit * 7)) # a space between words is 7 units

chill() {
	echo 0 > $light
	sleep $sameL
}

dash() {
	echo 1 > $light
	sleep $dah
}

dot() {
	echo 1 > $light
	sleep $dit
}

next() {
	echo 0 > $light
	sleep $letter
}

space() {
	echo 0 > $light
	sleep $word
}

dash # N
chill
dot
next
dot # E
next
dot # R
chill
dash
chill
dot
next
dash # D
chill
dot
chill
dot


echo 0 > $light
echo 11 > /sys/class/gpio/unexport
# -. . .-. -..
